import { useRouter } from 'next/router';
import { useState } from 'react';
import classes from './eventBox.module.css';

const EventBox = ({ event }) => {
  const router = useRouter();

  return (
    <div className={classes['event-box-container']} onClick={() => router.push('/sastanak/' + event._id)}>
      <p key={event._id}>{event.name}</p>
    </div>
  );
};

export default EventBox;
