import { useDispatch } from 'react-redux';
import { getUsers } from '../../store/slices/userSlice';

const Layout = (props) => {
  const dispatch = useDispatch();
  dispatch(getUsers());
  return <div>{props.children}</div>;
};

export default Layout;
