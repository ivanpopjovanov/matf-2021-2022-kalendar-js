import { createContext, useState } from 'react';
import MonthGrid from './MonthGrid';
import MonthPicker from './MonthPicker';
import WeekDaysHeader from './WeekDaysHeader';

const Calendar = () => {
  return (
    <div>
      <MonthPicker />
      <WeekDaysHeader />
      <MonthGrid />
    </div>
  );
};

export default Calendar;
