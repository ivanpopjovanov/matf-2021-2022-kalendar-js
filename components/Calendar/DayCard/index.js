import classes from './dayCard.module.css';
import Modal from '../../Modal';
import { useState } from 'react';
import EventBox from './EventBox';

const DayCard = ({ date, events, refreshEvents }) => {
  const [openModal, setOpenModal] = useState(false);

  const handleClick = (e) => {
    e.preventDefault();
    setOpenModal(true);
  };

  const onModalClose = () => {
    setOpenModal(false);
  };

  const datum = new Date(date);

  function sameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();
  }

  return (
    <div className={sameDay(new Date(), datum) ? classes['current-card'] : classes.card} onDoubleClick={handleClick}>
      <div className={classes.container}>
        <h1>{datum.getDate()}</h1>
        {events
          .filter((item) => sameDay(new Date(item.date), datum))
          .map((item) => (
            <EventBox key={item._id} event={item} />
          ))}
      </div>
      {openModal && <Modal onClose={onModalClose} eventDay={date} refreshEvents={refreshEvents} />}
    </div>
  );
};

export default DayCard;
