// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Kalendar', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

export default function handler(req, res) {
  res.status(200).json({ name: 'John Doe' });
}
