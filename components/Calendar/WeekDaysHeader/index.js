import gridClasses from "../MonthGrid/monthGrid.module.css";
import classes from "./weekDaysHeader.module.css";

const WeekDaysHeader = () => {

  const weekDays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

  return(
    <div className={gridClasses['grid-container']}>
      {
        weekDays.map((item) => (
          <div className={classes['week-day']} key={item}>{item}</div>
        ))
      }
    </div>
  )
}

export default WeekDaysHeader;