import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    currentDateString: (new Date()).toJSON(),
    displayMonth: (new Date()).getMonth(),
};

export const calendarSlice = createSlice({
  name: 'calendar',
  initialState,
  reducers: {
    increaseMonth: (state) => {
      if(state.displayMonth < 11)
        state.displayMonth += 1;
    },
    decreaseMonth: (state) => {
      if(state.displayMonth > 0)
        state.displayMonth -= 1;
    },
  },
});

export const { increaseMonth, decreaseMonth } = calendarSlice.actions;

export default calendarSlice.reducer;
