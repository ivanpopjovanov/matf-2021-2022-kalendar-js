import classes from './monthGrid.module.css';
import DayCard from '../DayCard';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

const MonthGrid = () => {
  const state = useSelector((state) => state);
  const [loading, setLoading] = useState(true);
  const [datumi, setDatumi] = useState([]);
  const [events, setEvents] = useState([]);

  useEffect(() => {
    setLoading(true);

    let pocetak = new Date(new Date().getFullYear(), state.calendar.displayMonth);
    pocetak.setDate(pocetak.getDate() - pocetak.getDay() + 1);

    let noviDatumi = [];
    let kraj = state.calendar.displayMonth + 1;
    if (kraj > 11) kraj = 0;
    while (pocetak.getMonth() != kraj) {
      noviDatumi = [...noviDatumi, pocetak.toString()];
      pocetak.setDate(pocetak.getDate() + 1);
    }

    setDatumi(noviDatumi);

    refreshEvents();

    // setTimeout(() => setLoading(false),1000);
    // setLoading(false);
  }, [state]);

  const refreshEvents = () => {
    setLoading(true);
    console.log('fetching http://localhost:3000/api/events');
    fetch('http://localhost:3000/api/events')
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
        setEvents(json);
      });
    setLoading(false);
  };

  return (
    <div className={classes['grid-container']}>
      {loading
        ? 'Loading...'
        : datumi.map((item) => <DayCard key={item} date={item} events={events} refreshEvents={refreshEvents} />)}
    </div>
  );
};

export default MonthGrid;
