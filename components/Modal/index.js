import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers, usersInfo } from '../../store/slices/userSlice';
import styles from './modal.module.css';
import { MultiSelect } from 'react-multi-select-component';

const Modal = ({ onClose, eventDay, refreshEvents }) => {
  const state = useSelector((state) => state);
  const { data, pending, error } = useSelector(usersInfo);

  useEffect(() => {
    console.log(data);
  }, []);

  const [name, setName] = useState('');
  const [time, setTime] = useState('00:00');
  const [desc, setDesc] = useState('');
  const [participants, setParticipants] = useState([]);

  const validateName = (name) => !!name;

  const validateTime = (time) => {
    let isValid = true;

    //TODO is this enough?
    isValid = !!time;

    return isValid;
  };

  return (
    <div className={styles.overlay} onClick={onClose}>
      <div
        className={styles.modal}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <form
          className={styles.form}
          onSubmit={(e) => {
            e.preventDefault();
            let isFormValid = true;

            if (!validateName(name)) {
              isFormValid = false;
            }

            if (!validateTime(time)) {
              isFormValid = false;
            }

            if (isFormValid) {
              const event = {
                name: name,
                date: new Date(new Date(eventDay).toDateString() + ' ' + time),
                description: desc,
                participants: participants.map((user) => user.value),
              };
              console.log(event.date);
              //localStorage.setItem('products', JSON.stringify([product]));
              fetch('http://localhost:3000/api/events', {
                body: JSON.stringify(event),
                headers: {
                  'Content-Type': 'application/json',
                },
                method: 'POST',
              }).then(refreshEvents());
              onClose();
            }
          }}
        >
          <div className={styles.group}>
            <label className={styles.label}>Event Name</label>
            <input
              className={`${styles.input} ${!validateName(name) ? styles.error : ''}`}
              type="text"
              placeholder="Enter event name..."
              value={name}
              onChange={(e) => name.length < 100 && setName(e.target.value)}
            />
          </div>
          <div className={styles.group}>
            <label className={styles.label}>Time</label>
            <input
              className={`${styles.input} ${!validateTime(time) ? styles.error : ''}`}
              type="time"
              value={time}
              onChange={(e) => setTime(e.target.value)}
            />
          </div>
          <div className={styles.group}>
            <label className={styles.label}>Participants</label>
            <MultiSelect
              options={
                data.length
                  ? data.map((user) => {
                      return { label: user.username, value: user._id };
                    })
                  : []
              }
              value={participants}
              onChange={setParticipants}
              labelledBy="Select"
            />
            {/* <select multiple>
              {data.map((item) => (
                <option key={item._id}>{item.username}</option>
              ))}
            </select> */}
          </div>
          <div className={styles.group}>
            <label className={styles.label}>Description</label>
            <textarea
              className={styles.input}
              placeholder="Enter event description..."
              rows={5}
              value={desc}
              onChange={(e) => setDesc(e.target.value)}
            ></textarea>
          </div>
          <button type="submit">Add new event</button>
        </form>
        {/* <button onClick={onClose}>Close</button> */}
      </div>
    </div>
  );
};

export default Modal;
