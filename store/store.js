import { configureStore } from '@reduxjs/toolkit';
import calendarReducer from './slices/calendarSlice';
import usersReducer from './slices/userSlice';

export default configureStore({
  reducer: {
    calendar: calendarReducer,
    users: usersReducer,
  },
});
