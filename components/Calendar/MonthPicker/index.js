import { useContext, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { increaseMonth, decreaseMonth } from '../../../store/slices/calendarSlice';
import classes from './monthPicker.module.css';

const MonthPicker = () => {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();

  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  return (
    <div className={classes['picker-container']}>
      <div>
        <button onClick={() => dispatch(decreaseMonth())}> &#60; </button>
      </div>
      <div>
        <h1>{monthNames[state.calendar.displayMonth]}</h1>
      </div>
      <div>
        <button onClick={() => dispatch(increaseMonth())}> &#62; </button>
      </div>
    </div>
  );
};

export default MonthPicker;
