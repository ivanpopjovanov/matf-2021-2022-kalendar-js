import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers, usersInfo } from '../../store/slices/userSlice';
const Sastanak = () => {
  const state = useSelector((state) => state);
  const { data, pending, error } = useSelector(usersInfo);

  const router = useRouter();
  const { id } = router.query;

  const [event, setEvent] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);

    refreshEvent();

    // setTimeout(() => setLoading(false),1000);
    // setLoading(false);
  }, [id]);

  const refreshEvent = () => {
    setLoading(true);
    console.log('fetching http://localhost:3000/api/events');
    fetch('http://localhost:3000/api/events')
      .then((res) => res.json())
      .then((json) => {
        console.log('json', json);
        console.log('id', id);
        const current = json.filter((obj) => obj._id === id);
        console.log('current', current);
        setEvent(current);
        setLoading(false);
      });
  };

  const deleteEvent = () => {
    fetch(`http://localhost:3000/api/events?id=${id}`, {
      method: 'DELETE',
    });
    router.push('/');
  };

  return (
    <div>
      {event.map((item) => (
        <div key={item._id}>
          <h1>{item.name}</h1>
          <p>{new Date(item.date).toString()}</p>
          <br />
          <button onClick={deleteEvent}>Delete</button>
          <br />
          <h2>Description:</h2>
          <p>{item.description}</p>
          <br />
          <br />
          <h2>Participants:</h2>
          {item.participants.map((user) => (
            // <p key={user}>{user}</p>
            <p key={user}>{data.filter((userData) => userData._id === user)[0].username}</p>
          ))}
        </div>
      ))}
    </div>
  );
};

export default Sastanak;
